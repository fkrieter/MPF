# MPF - The M plotting framework

# Setup

Add MPF module to your PYTHONPATH (https://askubuntu.com/a/250935) or run

```sh
source setup.sh
```

For examples have a look at the python scripts in examples. These should be standalone scripts that can be executed - for example:

```sh
cd examples
./plot.py
```

# Tests

To run all the unit tests type

```sh
python -m MPF.test
```

This will run automated image comparison tests which will currently only succeed in a specific setup. You can use the docker container [nikolaihartmann/root-matplotlib](https://hub.docker.com/r/nikolaihartmann/root-matplotlib) to run the tests.

For running them in any other setup you can use `--pdf` to create pdf collections which you can manually look at:

```sh
python -m MPF.test --pdf
```

# Documentation

To generate the documentation run

```sh
cd doc
./generate_images.py
make html
```

To watch the documentation open `doc/_build/html/index.html` in your Browser

The documentation is also available via http://mpf-plotting.rtfd.io

# Dependencies

## ROOT (Please use at least version 6)
http://root.cern.ch

We don't maintain ROOT5 support, but so far it seems it is still working, with the following exceptions:

* You have to use `compile=True` in `HistProjector.fillHists` or `TreePlotter.plotAll` if using MultiHistDrawer
* The example tree generation code won't work
* Because of the above 2 issues, the unit tests won't suceed with ROOT5

## python/pyROOT

We maintain support for ROOT compiled with support for python2 (at least 2.7) and python3

## meme
The package makes extensive use of the caching provided by the
[meme](https://gitlab.com/nikoladze/meme) module.  
It is included in the package (subtree), so it doesn't
need to be checked out.

Current version: [`starwars-kid (1.1)`](https://gitlab.com/nikoladze/meme/tags/1.1)

Examples to interact with the meme remote:

Add remote  
```sh
git remote add meme git@gitlab.com:nikoladze/meme.git
```  
Fetch changes  
```sh
git fetch meme
```  
Diff the currently checked out subtree  
```sh
git diff HEAD:meme meme/master
```
Pull in new another version (merge works as well). Due to `--squash` it also works backwards  
```sh
git subtree pull -P meme meme <ref> --squash
```