import unittest
import itertools
import os
import shutil

import ROOT

from ..canvas import Canvas
from ..legend import Legend
from ..histograms import HistogramD
from .. import pyrootHelpers as PH

from .helpers.pdfCollectionTest import PDFCollectionTest

from ..commonHelpers.logger import logger
logger = logger.getChild(__name__)

class Test(PDFCollectionTest):

    @classmethod
    def setUpClass(cls):
        cls.mergedName = "legendTests.pdf"
        super(Test, cls).setUpClass()


    @staticmethod
    def generateLegend(nColumns, nEntries, **kwargs):
        titlePrefix = kwargs.pop("titlePrefix", "title")
        l = Legend(nColumns=nColumns, **kwargs)
        hists = []
        for i in range(nEntries):
            tempName = next(PH.tempNames)
            o = HistogramD(ROOT.TH1D(tempName, "{} {}".format(titlePrefix, i), 1, 0, 2))
            o.SetFillColor(i)
            l.addEntry(o, "f")
            hists.append(o)
        return l, hists


    def test_1col(self):
        c = Canvas(splitting=None)
        c.pads["main"].debugText = ["Test 1 column legend"]
        l, _ = self.generateLegend(1, 5)
        c.pads["main"].legend = l
        self.pdf = next(self.pdfNames)
        c.saveAs(self.pdf)


    def test_2col(self):
        c = Canvas(splitting=None)
        c.pads["main"].debugText = ["Test 2 column legend"]
        l, _ = self.generateLegend(2, 5)
        c.pads["main"].legend = l
        self.pdf = next(self.pdfNames)
        c.saveAs(self.pdf)

    def test_3col(self):
        c = Canvas(splitting=None)
        c.pads["main"].debugText = ["Test 3 column legend"]
        l, _ = self.generateLegend(3, 5)
        c.pads["main"].legend = l
        self.pdf = next(self.pdfNames)
        c.saveAs(self.pdf)

    def test_manyEntries(self):
        c = Canvas(splitting=None)
        c.pads["main"].debugText = ["Test many entries (should be scaled)"]
        l, _ = self.generateLegend(1, 30)
        c.pads["main"].legend = l
        self.pdf = next(self.pdfNames)
        c.saveAs(self.pdf)


    def test_longTitle(self):
        c = Canvas(splitting=None)
        c.pads["main"].debugText = ["Test long titles (should be scaled and only 1 column)"]
        l, _ = self.generateLegend(2, 5, titlePrefix="this is a very long title")
        c.pads["main"].legend = l
        self.pdf = next(self.pdfNames)
        c.saveAs(self.pdf)

    def test_splitCanvas(self):
        c = Canvas(splitting='ratio')
        c.pads["main"].debugText = ["Test if all legends have the same absolute text/box sizes"]
        l1, h1 = self.generateLegend(1, 5, xOffset=0.1)
        l2, h2 = self.generateLegend(2, 5, xOffset=-0.07)
        l3, h3 = self.generateLegend(3, 5, xOffset=-0.4)
        for pad in [c.pads["main"], c.pads["bottom"]]:
            pad.cd()
            l1.draw()
            l2.draw()
            l3.draw()
        self.pdf = next(self.pdfNames)
        c.saveAs(self.pdf)

    def test_scaleLegend(self):
        c = Canvas(splitting=None)
        c.pads["main"].debugText = ["Test if all legends are scaled by 0.7"]
        l1, h1 = self.generateLegend(1, 5, xOffset=0.1, scaleLegend=0.7)
        l2, h2 = self.generateLegend(2, 5, xOffset=-0.07, scaleLegend=0.7)
        l3, h3 = self.generateLegend(3, 5, xOffset=-0.4, scaleLegend=0.7)
        pad = c.pads["main"]
        pad.cd()
        l1.draw()
        l2.draw()
        l3.draw()
        self.pdf = next(self.pdfNames)
        c.saveAs(self.pdf)

if __name__ == "__main__":

    unittest.main()
