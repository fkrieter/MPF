import unittest
from .is_close import isclose

from ...commonHelpers.logger import logger
logger = logger.getChild(__name__)

class FloatTester(unittest.TestCase):

    """Base Test for everything that needs to compare floats, histograms etc"""

    def assertClose(self, a, b):
        """Test if two floats are equal (relative difference < 1e-9, using
        :py:meth:`~MPF.test.helpers.is_close.isclose`)"""
        logger.debug("{} ?= {}".format(a, b))
        self.assertTrue(isclose(a, b))


    def assertEqualHists(self, hist1, hist2):
        """Test if two histograms are equal (bin contents equal according to
        :py:meth:`~MPF.test.helpers.floatTester.FloatTester.assertClose`"""

        self.assertEqual(hist1.GetNbinsX(), hist2.GetNbinsX())
        for i in range(hist1.GetNbinsX()+1):
            content1 = hist1.GetBinContent(i)
            content2 = hist2.GetBinContent(i)
            self.assertClose(content1, content2)
            error1 = hist1.GetBinError(i)
            error2 = hist1.GetBinError(i)
            self.assertClose(error1, error2)
        self.assertClose(hist1.Integral(), hist2.Integral())
