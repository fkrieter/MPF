import unittest
from array import array

import ROOT

from MPF import pyrootHelpers as PH
from MPF.plotStore import PlotStore
from MPF.atlasStyle import setAtlasStyle
from MPF.canvas import Canvas
from .helpers.pdfCollectionTest import PDFCollectionTest

class TestRootStyleColor(unittest.TestCase):

    def test_root(self):
        self.assertEqual(PH.rootStyleColor(ROOT.kBlack), 1)
        self.assertEqual(PH.rootStyleColor(ROOT.kRed+1), 633)

    def test_root_string(self):
        self.assertEqual(PH.rootStyleColor('kRed+1'), 633)

    def test_hex_string(self):
        # the outcome of this depends somehow on the palette that was
        # set at some point. However, it seems we can't force a
        # particular value here by manually setting the palette
        # ... ROOT is weird
        self.assertEqual(PH.rootStyleColor('#6baff4'), ROOT.TColor.GetColor("#6baff4"))


class TestHistFromGraph(PDFCollectionTest):

    @classmethod
    def setUpClass(cls):
        setAtlasStyle()
        super(TestHistFromGraph, cls).setUpClass()
        cls.mergedName = "pyrootHelpers.pdf"

    def test_histFromGraph_xerror(self):
        g = ROOT.TGraphAsymmErrors()
        for i in range(10):
            n = g.GetN()
            g.SetPoint(n, i, i)
            g.SetPointError(n, 0.5, 0.5, 1, 1)
        h = PH.histFromGraph(g)
        h.SetLineColor(ROOT.kBlue)
        g.SetFillColor(ROOT.kRed)
        c = Canvas(splitting=None)
        c.pads["main"].debugText = ["test_histFromGraph_xerror"]
        c.pads["main"].cd()
        g.Draw("ae2")
        h.Draw("same")
        self.pdf = c.saveAs(next(self.pdfNames))


    def test_histFromGraph_varBinSize(self):
        g = ROOT.TGraphAsymmErrors()
        bins = array('d', [1, 2, 4, 7, 11, 16])
        h1 = ROOT.TH1D("h1", "", len(bins)-1, bins)
        h2 = ROOT.TH1D("h2", "", len(bins)-1, bins)
        for i in range(16):
            h1.Fill(i)
            if i > 6:
                h2.Fill(i)
        g.Divide(h2, h1)
        h = PH.histFromGraph(g)
        h.SetLineColor(ROOT.kBlue)
        g.SetFillColor(ROOT.kRed)
        c = Canvas(splitting=None)
        c.pads["main"].debugText = ["test_histFromGraph_varBinSize"]
        c.pads["main"].cd()
        g.Draw("ae2")
        h.Draw("same")
        self.pdf = c.saveAs(next(self.pdfNames))

